import os

from tortoise import fields, models


class Message(models.Model):
    """
    The Message model.
    """

    id = fields.UUIDField()
    url = fields.CharField(max_length=255)
    title = fields.CharField(max_length=255)
    publication_date = fields.DateField()
    title = fields.CharField(max_length=255)
    title = fields.CharField()

    def __str__(self):
        return self.title

    class Meta:
        app = os.getenv("APP_NAME")
        ordering = ["title"]
