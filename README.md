# Scrapy Demo

Demo project to test Scrapy framework functionality.

## Task

- Write a spider using Scrapy framework to extract at least 10 items from
  `https://www.bwcon.de/aus-dem-netzwerk/meldungen`
- Each item should contain the following fields: `url`, `title`, `publication_date`, `description` and `body`.

## Running Dev

1. `pip install pdm`
2. `eval "$(pdm --pep582)"`
3. `pdm install`
4. Run from the debugger in your IDE (required).

- A debug trace is required to load the .env environment variables.

## Running Prod

1. `bash build_dev.sh`
2. `docker compose up -d`
3. `docker logs scrapy-demo`
