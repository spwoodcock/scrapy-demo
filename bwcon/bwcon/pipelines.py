from tortoise import Tortoise, run_async

from bwcon.settings import DATABASE
from bwcon.models import Message


async def db_init():
    await Tortoise.init(db_url=DATABASE.DB_URL, modules={"models": ["bwcon.models"]})
    await Tortoise.generate_schemas()


class BwconPipeline:
    def __init__(self):
        """
        Initializes database connection and creates items table.
        """

        run_async(db_init())

    async def process_item(self, item, spider):
        """
        Process the item and store to database.
        """

        Message.get_or_create(**item)
        await Message.save()

        return item
