#!/bin/bash

source .env
docker build . \
    --target debug \
    --tag "${APP_NAME}:${APP_VERSION}-debug" \
    --build-arg APP_VERSION=${APP_VERSION} \
    --build-arg PYTHON_IMG_TAG=${PYTHON_IMG_TAG} \
    --build-arg MAINTAINER=${MAINTAINER} \
    --build-arg EXTERNAL_REG=${EXTERNAL_REG}
